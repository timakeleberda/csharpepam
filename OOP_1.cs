class car
{
    private string name;
    private int max_speed;
    public car()
    {
        Console.WriteLine("Введите имя автомобиля.");
        this.name = Console.ReadLine();
        Console.WriteLine("Введите максимальную скорость автомобиля.");
        int buf_speed = Int32.Parse(Console.ReadLine());
        this.max_speed = buf_speed > 0 ? buf_speed : 0;
    }
    public string Name
    {
        get
        {
            return name;
        }
        set
        {
            name = value;
        }
    }
    public int Max_speed
    {
        get
        {
            return max_speed;
        }
        set
        {
            if (value > 0)
                max_speed = value;
            else
                max_speed = 0;
            Console.WriteLine("Ошибка!!!");
        }
    }
 


}
class Program
{
    static void Main(string[] args)
    {
        car car_1 = new car();
        car car_2 = new car();
        object_car_max_speed(car_1, car_2);
    }
    static void object_car_max_speed(car car_1, car car_2)
    {
        if (car_1.Max_speed > car_2.Max_speed)
        {
            Console.WriteLine($"У {car_1.Name}, больше максимальная скорость.");
        }
        else if (car_1.Max_speed < car_2.Max_speed)
        {
            Console.WriteLine($"У {car_2.Name}, больше максимальная скорость.");
        }
        else
        {
            Console.WriteLine($"У {car_1.Name} и {car_2.Name}, одинаковая максимальная скорость.");
        }
    }
}
