//Methods

static double fromStringToDouble(string value) //take 6 feet 0 inches to 6.0
{
    double output = 0.0;
    try
    {
        output += Convert.ToDouble(value);
    }
    catch(Exception)
    {
        if (value.Contains("inch") != true)
        {
            output += Convert.ToDouble(new string(value.TakeWhile(char.IsDigit).ToArray()));//https://stackoverflow.com/questions/9080492/get-first-numbers-from-string
        } else if (value.Contains("inch"))
        {
            string fpart = value.Substring(0, value.IndexOf("t"));
            string spart = value.Substring(value.IndexOf("t")+2); 
            output += Convert.ToDouble(new string(fpart.TakeWhile(char.IsDigit).ToArray()));
            output += 0.08*Convert.ToDouble(new string(spart.TakeWhile(char.IsDigit).ToArray()));
        }

    }
    return output;
}

static List<double> takeValues(string name) //input values from user
{
    List<double> list = new List<double>();
    Console.WriteLine($"{name} write cost 1/1 feet (weight and height = 1), please"); 
    //price of 1/1 feet
    list.Add(fromStringToDouble(Console.ReadLine()));
    Console.WriteLine($"{name} write weight, please"); 
    //weight
    list.Add(fromStringToDouble(Console.ReadLine()));
    Console.WriteLine($"{name} write height, please"); 
    //height
    list.Add(fromStringToDouble(Console.ReadLine()));
    return list;
}// end of takeValues, i look up how to use List<T>

static double calculations(string name, List<double> list) //calculations
{
    double squareRes = list[0]*list[1]*list[2]; //price of 1 wall

    bool checker = true; // for while
    while (checker)
    {
        Console.WriteLine($"{name}, it's your cost:{squareRes}$ of 1 wall\n 1.More then 1 wall\n2.Quit and save price\n3.Quit without save price");
        switch (Convert.ToInt32(Console.ReadLine()))
        {
            case 1:
                Console.WriteLine($"{name}, how many walls you want to do?");
                return squareRes * Convert.ToDouble(Console.ReadLine());
                break;
            case 2:
                return squareRes;
                break;
            case 3: 
                return 0.0;
                break;
        }
    }
    return squareRes;

}//end of calculations

//Call methods and start of program
//basic values
double finalcost = 0.0;
string namePerson;

//Take a name
Console.WriteLine("Hello! What is your name?");
namePerson = Console.ReadLine();
// Run program
bool checkerbox = true; //for while
while (checkerbox)
{
    Console.WriteLine($"{namePerson}, what do you want to do:\n1.Calculate wall\n2.Enter price to final of 1 item(for example instrument)\n3.Show final price \n4.End\n Write '1' if you wanna calculate wall or '4' if you wanna exit");
    switch (Convert.ToInt32(Console.ReadLine()))
    {
        case 1: //1.Calculate wall
            finalcost += calculations(namePerson, takeValues(namePerson));
            break;
        case 2: //2.Enter price to final of 1 item
            Console.WriteLine("How many did you spend for this item or items");
            finalcost += Convert.ToDouble(Console.ReadLine());
            break;
        case 3://3.Show final price
            Console.WriteLine($"{namePerson} your finalprice now: {finalcost}$!");
            break;
        case 4: //4.End           
            Console.WriteLine($"Your finalprice is {finalcost}$. Have a great day {namePerson}!");
            checkerbox = false;
            break;
    }
    
}// end of while and end of program
