interface ICut
{
    public string cut();
}
abstract class Knifes 
{
    public abstract string Name { get; set; }
    public abstract int Age { get; set; }
    public abstract void Aboutme();

}
class NewKnife : Knifes, ICut
{
    public override string Name { get; set; }
    public override int Age { get; set; }
    public string cut()
    {
        return "Я умею резать." ;
    }
    public override void Aboutme()
    {
        Console.WriteLine($"Я {Name} мне {Age} лет и {cut()}");
    }
    public NewKnife(string name, int age)
    {
        Age = age;
        Name = name;
    }
}
class OldKnife : Knifes
{
    public override string Name { get; set; }
    public override int Age { get; set; }
    public override void Aboutme()
    {
        Console.WriteLine($"Я {Name} мне {Age} лет и я умел резать");
    }
    public OldKnife(string name, int age) { 
        Age = age;
        Name = name;
    }
}
class Program
{
    static void Main(string[] args)
    {
        NewKnife obj_1 = new NewKnife("новый нож", 5);
        OldKnife obj_2 = new OldKnife("старый нож",100);
        obj_1.Aboutme();
        obj_2.Aboutme();
    }
}
