﻿using System;

public class Start_st_of_Csharp
{
    public static void Main()
    {
        int[] res_mas = con_us();
        if (res_mas[0] == 0)
        {
            Console.WriteLine("Введите 1 или 2");
        }
        else if (res_mas[0] == 1)
        {
            avg(res_mas[1], res_mas[2]);
        }
        else if(res_mas[0] == 2)
        {
            Console.WriteLine($"Максимальное число в рандомном массиве:{max_mas1(gen_mas1(res_mas[1], res_mas[2], res_mas[3]))}");
        }
        
    }
    static int[] gen_mas1(int x, int i, int y)
    {
        int[] mas = new int[x];
        Random rnd = new Random();
        for (int z = 0; z < mas.Length; z++)
        {
            mas[z] = rnd.Next(i, y);
        }
        return mas;
    }
    static void avg(int a, int b)
    {
        Console.WriteLine($"Среднее арифметическое двух чисел:{(a + b) / 2}");
    }
    static int max_mas1(int[] mas)
    {
        int max = mas[0];
        foreach (int i in mas)
        {
            if (i > max)
            {
                max = i;
            }
        }
        return max;
    }
    static int[] con_us()
    {
        int[] res_mas = new int[4];
        Console.WriteLine(@"Введите 1 или 2 для выбора программы
1.Найти среднее арифметическое среди двух чисел.
2.Найти максимальное число в масиве.");
        int res = int.Parse(Console.ReadLine());
        res_mas[0] = res;
        if (res == 1)
        {
            
            Console.WriteLine("Введите первое число:");
            res_mas[1] = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите второе число:");
            res_mas[2] = int.Parse(Console.ReadLine());
            return res_mas;
        }
        if (res == 2)
        {
            Console.WriteLine("Введите длину рандомного массива:");
            res_mas[1] = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите от какого числа генерировать рандомные числа:");
            res_mas[2] = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите до какого числа генерировать рандомные числа:");
            res_mas[3] = int.Parse(Console.ReadLine());
            return res_mas;
        }
        return res_mas;
    }
}