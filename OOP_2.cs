class Automobile
{
    public string title;
    public int maxspeed;
    public string Title
    {
        get;
    }
    public int Maxspeed
    {
        get;
    }
    public Automobile(string title, int maxspeed)
    {
        Title = title;
        Maxspeed = maxspeed;
    }
    public virtual void AboutAM()
    {
        Console.WriteLine($"Це {title} і він має максимальну швидкисть {maxspeed} км/год ");
    }

}
class Vehile : Automobile
{
    public int max_c;
    public int Max_c
    {
        get;
    }
    public Vehile(string title,int maxspeed, int max_c):base (title,maxspeed)
    { 
        Max_c = max_c;
    }
    public override void AboutAM()
    {
        Console.WriteLine($"Це {Title} його маскимальна швидкість: {Maxspeed}км/год і максимальна кількість вантажу у вантажньому авто:{Max_c}");
    }

}
class Car : Automobile
{
    public int max_p;
    public int Max_p
    {
        get;
    }
    public Car(string title, int maxspeed, int max_p) : base(title, maxspeed)
    {
        Max_p = max_p;
    }
    public override void AboutAM()
    {
        Console.WriteLine($"Це {Title} його маскимальна швидкість: {Maxspeed} і максимальна кількість пассажирів у Авто:{Max_p}");
    }
}
class Program
{
    static void Main(string[] args)
    {
        Automobile[] cars =
        {
            new Vehile("Mitsubishi",200,1),
            new Car("Mersedes",350,4)
        };
        if (cars[0].Maxspeed > cars[1].Maxspeed)
        {
            cars[0].AboutAM();
        }
        else if(cars[0].Maxspeed < cars[1].Maxspeed)
        {
            cars[1].AboutAM();
        }
        else
        {
            Console.WriteLine($"В автомобіля {cars[0].Title} і {cars[1].Title} - однакова швидкість");
        }
    }
}
